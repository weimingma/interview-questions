package com.mastercard.labs.mcea.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("/cards")
public class CardsController {
 @GetMapping(value = "/min-max", produces = MediaType.APPLICATION_JSON_VALUE)
 public ResponseEntity findMinMax(HttpServletRequest request) {
    return ResponseEntity.ok().build();
 }
}

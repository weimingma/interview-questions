package com.mastercard.labs.mcea.controllers;

import com.mastercard.labs.mcea.conf.AppConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("/health")
public class HealthController {

    private final AppConfig appConfig;

    @GetMapping(value = "/ping", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map ping(HttpServletRequest request) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ping", true);
        map.put("appName", appConfig.getAppName());
        map.put("version", appConfig.getVersion());
        map.put("serviceName", request.getServerName());
        map.put("referrer", request.getHeader("referer"));
        return map;
    }
}

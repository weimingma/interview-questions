package com.mastercard.labs.mcea.conf;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {
  private String version;
  private String appName;
}

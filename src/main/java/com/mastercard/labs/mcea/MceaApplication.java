package com.mastercard.labs.mcea;

import com.mastercard.labs.mcea.conf.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties({AppConfig.class})
@SpringBootApplication
public class MceaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MceaApplication.class, args);
	}
}

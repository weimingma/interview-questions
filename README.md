# Project Onboarding

This is a Spring Boot application (with maven) serving as our backend API server.


## Getting Started

## Running on a local machine

### Installing the dependencies.

    mvn clean package
    
### Running the application.

    mvn spring-boot:run
    
### Check the application is running by visiting this endpoint:

    http://localhost:8080/actuator/health
    
# Assignment: Find the minimum and maximum card numbers.

## Summary

Implement the service endpoint `/cards/min-max` to return the minimum and maximum value of EAIDs in the given CSV file named `eaids.csv`.

## Description

You are given a file named `eaids.csv` in the `resources` folder containing information given by a bank.

This first 3 rows contains meta data about the file, and can be ignored in this assignment.

The 4th row is the headers of 2 columns, "last 4 digits" and "EAIDS".

From the 5th row onwards, there are 125 EAID records representing credit card numbers. Each EAID value is a string of 30 characters long.

The card number is the last 10 digits of EAID.

For example, an EAID with a value of :

    08401A103030000000000100016221
    
Has a card number of:

    0100016221
    
We would like to find out what is the minimum and maximum card number in this sample file.

Implement the endpoint `/cards/min-max` (in `CardsController`) to read the CSV file, find out the minimum and maximum card numbers, and return as a JSON response.




## Acceptance criteria
    
1. Send a GET request to `/cards/min-max` without any errors.
2. Return a HTTP response with 200 status code.
3. The response should be in JSON format.
5. The JSON response should contain 2 keys: `min` and `max`, the values of the minimum and maximum card numbers.
6. The card numbers is to be returned as a string object of 10 characters long. If the card number is less than 10 characters long, pad the card number with "0s" in front and return as a String.

## Example:

Input:

    curl http://localhost:8080/cards/min-max
    
Output
    
    {
        "min": "0100000166",
        "max": "0100019936"
    }